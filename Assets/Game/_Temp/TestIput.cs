using UnityEngine;
using WKosArch.Common.DIContainer;
using WKosArch.Extentions;
using WKosArch.Services.InputService;

public class TestIput : MonoBehaviour
{
    private IInputService _inputService;

    private void OnEnable()
    {
        _inputService = new DIVar<IInputService>().Value;

        _inputService.OnThrottleBrakeEvent += PrintBrakeValue;
        _inputService.OnSterringWheelEvent += PrintWheelValue;
    }
    private void OnDisable()
    {
        _inputService.OnThrottleBrakeEvent -= PrintBrakeValue;
        _inputService.OnSterringWheelEvent -= PrintWheelValue;
    }

    private void PrintBrakeValue(Vector2 vector2)
    {
        Log.PrintColor($"PrintBrakeValue = {vector2}", Color.yellow);
    }

    private void PrintWheelValue(Vector2 vector2)
    {
        Log.PrintColor($"PrintWheelValue = {vector2}", Color.yellow);

    }
}
