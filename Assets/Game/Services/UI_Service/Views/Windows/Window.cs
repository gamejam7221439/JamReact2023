﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using WKosArch.Services.UIService.Common;

namespace WKosArch.UIService.Views.Windows
{
    public abstract class Window<TWindowViewModel> : View<TWindowViewModel>, IWindow<TWindowViewModel>
        where TWindowViewModel : WindowViewModel
    {
        public event Action<WindowViewModel> Hidden;
        public event Action<WindowViewModel> Destroyed;

        [SerializeField] private Transition _transitionIn = default;
        [SerializeField] private Transition _transitionOut = default;
        [Space]
        [SerializeField] private Button _backButton;

        public bool IsShown { get; private set; }

        public async UniTask<IWindow> Show()
        {
            IsShown = true;

            gameObject.SetActive(true);
            transform.SetAsLastSibling();

            if (_transitionIn != null)
            {
                await _transitionIn.Play();
            }

            return this;
        }

        public async UniTask<IWindow> Hide(bool forced = false)
        {
            if (_transitionIn != null && _transitionIn.IsPlaying && !forced)
            {
                return this;
            }

            if (_transitionOut != null && !forced)
            {
                if (_transitionOut.IsPlaying)
                {
                    return this;
                }

                await _transitionOut.Play();
            }

            HideInstantly();

            IsShown = false;
            return this;
        }

        public IWindow HideInstantly()
        {
            if (ViewModel.WindowSettings.IsPreCached)
            {
                gameObject.SetActive(false);
            }
            else
            {
                Destroy(gameObject);
            }

            Hidden?.Invoke(ViewModel);
            return this;
        }

        protected virtual void OnDestroy()
        {
            Destroyed?.Invoke(ViewModel);
        }

        public override void Subscribe()
        {
            base.Subscribe();
            _backButton?.onClick.AddListener(ViewModel.Back);
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            _backButton?.onClick.RemoveListener(ViewModel.Back);
        }
    }
}