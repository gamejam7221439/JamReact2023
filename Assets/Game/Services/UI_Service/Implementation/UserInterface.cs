using Assets.LocalPackages.WKosArch.Scripts.Common.DIContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WKosArch.Extentions;
using WKosArch.Services.UIService.Common;
using WKosArch.UIService.Views.HUD;
using WKosArch.UIService.Views.Windows;

namespace WKosArch.Services.UIService
{
    public class UserInterface : MonoBehaviour
    {
        private const string PrefabPath = "[INTERFACE]";

        public event Action<WindowViewModel> WindowOpened;
        public event Action<WindowViewModel> WindowClosed;

        [SerializeField] private UILayerContainer[] _containers;

        public WindowViewModel FocusedWindowViewModel { get; private set; }
        public Camera UICamera { get; private set; }

        private static UserInterface _instance;
        private UISceneConfig _uiSceneConfig;
        private Dictionary<Type, WindowViewModel> _createdWindowViewModelsCache = new Dictionary<Type, WindowViewModel>();
        private WindowsStack _windowStack = new WindowsStack();

        private static IDIContainer _diContainer;

        public static UserInterface CreateInstance(IDIContainer container)
        {
            if (_instance != null)
            {
                Debug.LogWarning($"UserInterface CreateInstance _instance = {_instance}");
                return _instance;
            }

            _diContainer = container;

            var prefab = Resources.Load<UserInterface>(PrefabPath);
            _instance = Instantiate(prefab);
            DontDestroyOnLoad(_instance);

            return _instance;
        }

        private void Awake()
        {
            UICamera = GetComponentInChildren<Camera>();
        }

        public void Build(UISceneConfig config)
        {
            _uiSceneConfig = config;

            DestroyOldWindows();
            CreateNewWindows();
        }
       

        public void ShowWindow<T>() where T : WindowViewModel
        {
            var windowViewModelType = typeof(T);

            WindowViewModel windowViewModel;

            if (_createdWindowViewModelsCache.TryGetValue(windowViewModelType, out windowViewModel))
            {
                ActivateWindowViewModel(windowViewModel);
            }
            else
            {
                _uiSceneConfig.TryGetPrefab(out T prefab);

                if (prefab == null)
                {
                    Log.PrintWarning($"Couldn't open window ({windowViewModelType}). Maybe its not add to UISceneConfig for this Scene");
                }

                windowViewModel = CreateWindowViewModel(prefab);

                ActivateWindowViewModel(windowViewModel);
            }
        }

        public void Back(bool hideCurrentWindow = true)
        {
            if (FocusedWindowViewModel.Window is IHomeWindow)
            {
                return;
            }

            if (hideCurrentWindow)
            {
                FocusedWindowViewModel.Window.Hide();

            }
            _windowStack.Pop();


            var windowTypeForRefreshing = _windowStack.Pop();
            var viewModelForRefreshing = _createdWindowViewModelsCache[windowTypeForRefreshing];

            ActivateWindowViewModel(viewModelForRefreshing);
        }

        public Transform GetContainer(UILayer layer)
        {
            return _containers.FirstOrDefault(container => container.layer == layer)?.transform;
        }

        private WindowViewModel CreateWindowViewModel(WindowViewModel prefabWindowViewModel)
        {

            var windowViewModelType = prefabWindowViewModel.GetType();

            if (_createdWindowViewModelsCache.TryGetValue(windowViewModelType, out var windowViewModel))
            {
                return windowViewModel;
            }

            var container = GetContainer(prefabWindowViewModel.WindowSettings.TargetLayer);
            var createdWindowViewModel = Instantiate(prefabWindowViewModel, container);
            createdWindowViewModel.InjectDI(_diContainer);


            _createdWindowViewModelsCache[windowViewModelType] = createdWindowViewModel;


            if (createdWindowViewModel.WindowSettings.OpenWhenCreated)
            {
                ActivateWindowViewModel(createdWindowViewModel);
            }
            else
            {
                createdWindowViewModel.Window.HideInstantly();
            }


            return createdWindowViewModel;
        }

        private void ActivateWindowViewModel(WindowViewModel windowViewModel)
        {
            FocusedWindowViewModel = windowViewModel;

            windowViewModel.Refresh();

            if (!windowViewModel.Window.IsShown)
            {
                windowViewModel.Subscribe();

                var window = windowViewModel.Window;

                window.Show();
                window.Hidden += OnWindowHidden;
                window.Destroyed += OnWindowDestroyed;

                WindowOpened?.Invoke(FocusedWindowViewModel);
            }


            if (windowViewModel is not IHud)
                _windowStack.Push(windowViewModel.GetType());
        }

        private void DestroyOldWindows()
        {
            foreach (var createdWindowViewModelItem in _createdWindowViewModelsCache)
            {
                Destroy(createdWindowViewModelItem.Value.gameObject);
            }

            _createdWindowViewModelsCache.Clear();

            _windowStack.Clear();
        }

        private void CreateNewWindows()
        {
            FocusedWindowViewModel = null;

            var prefabsForCreating = _uiSceneConfig.WindowPrefabs;

            foreach (var prefab in prefabsForCreating)
            {
                if (prefab.WindowSettings.IsPreCached)
                {
                    CreateWindowViewModel(prefab);
                }
            }
        }

        private void OnWindowDestroyed(WindowViewModel windowViewModel)
        {
            var window = windowViewModel.Window;

            _windowStack.RemoveLast(windowViewModel.GetType());

            window.Destroyed -= OnWindowDestroyed;
            window.Hidden -= OnWindowHidden;

            if (!windowViewModel.WindowSettings.IsPreCached)
            {
                var windowViewModelType = windowViewModel.GetType();

                _createdWindowViewModelsCache.Remove(windowViewModelType);
            }
        }

        private void OnWindowHidden(WindowViewModel windowViewModel)
        {
            windowViewModel.Unsubscribe();

            var focusedWindowType = _windowStack.GetLast();
            var focusedWindowViewModel = _createdWindowViewModelsCache[focusedWindowType];

            FocusedWindowViewModel = focusedWindowViewModel;

            WindowClosed?.Invoke(windowViewModel);
        }

    }
}