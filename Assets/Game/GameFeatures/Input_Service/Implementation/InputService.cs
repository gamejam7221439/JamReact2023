using UnityEngine;

namespace WKosArch.Services.InputService
{
    public class InputService : IInputService
    {
        public delegate void Vector2Value(Vector2 vector2);

        public event Vector2Value OnSterringWheelEvent;
        public event Vector2Value OnThrottleBrakeEvent;
        public bool IsReady => _isReady;

        private bool _isReady;
        private PlayerInputHandler _playerInputHandler;

        public InputService()
        {
            _playerInputHandler = PlayerInputHandler.CreateInstance();
            _playerInputHandler.InjectDI(this);

            _isReady = true;
        }

        public void OnWheel(Vector2 vector2)
        {
            OnSterringWheelEvent?.Invoke(vector2);
        }

        public void OnThrottleBrake(Vector2 vector2)
        {
            OnThrottleBrakeEvent?.Invoke(vector2);
        }
    }

}