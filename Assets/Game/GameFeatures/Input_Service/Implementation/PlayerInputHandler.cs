using UnityEngine;
using UnityEngine.InputSystem;

namespace WKosArch.Services.InputService
{
    public class PlayerInputHandler : MonoBehaviour
    {
        private const string PrefabPath = "[InputService]";

        [SerializeField] private PlayerInput _playerInput;

        private IInputService _inputService;


        public static PlayerInputHandler CreateInstance()
        {
            var prefab = Resources.Load<PlayerInputHandler>(PrefabPath);
            var inputHandler = Instantiate(prefab);

            DontDestroyOnLoad(inputHandler.gameObject);

            return inputHandler;
        }

        public void InjectDI(IInputService inputService)
        {
            _inputService = inputService;
        }

        public void OnWheel(InputValue inputValue)
        {
            var vector2 = inputValue.Get<Vector2>();

            _inputService.OnWheel(vector2);
        }

        public void OnThrottleBrake(InputValue inputValue)
        {
            var vector2 = inputValue.Get<Vector2>();

            _inputService.OnThrottleBrake(vector2);
        }
    } 
}
