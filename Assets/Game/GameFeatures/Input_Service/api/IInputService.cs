﻿using UnityEngine;
using WKosArch.Domain.Features;

namespace WKosArch.Services.InputService
{
    public interface IInputService : IFeature
    {
        event InputService.Vector2Value OnSterringWheelEvent;
        event InputService.Vector2Value OnThrottleBrakeEvent;

        void OnThrottleBrake(Vector2 vector2);
        void OnWheel(Vector2 vector2);
    } 
}