using Assets.LocalPackages.WKosArch.Scripts.Common.DIContainer;
using UnityEngine;
using WKosArch.Domain.Contexts;
using WKosArch.Domain.Features;
using WKosArch.Extentions;

namespace WKosArch.Services.InputService
{
    [CreateAssetMenu(fileName = "InputService_Installer", menuName = "Game/Installers/InputService_Installer")]
    public class InputService_Installer : FeatureInstaller
    {
        private IInputService _service;

        public override IFeature Create(IDIContainer container)
        {
            _service = new InputService();

            container.Bind(_service);

            Log.PrintColor($"[IInputService] Create and Bind", Color.cyan);
            return _service;
        }

        public override void Dispose()
        {

        }
    }

}