﻿using UnityEngine;
using UnityEngine.UI;
using WKosArch.UIService.Views.Windows;

public class WindowSettingButton : Window<SettingButtonViewModel>, IHomeWindow
{
    [Space]
    [SerializeField] private Button _settingButton;

    public override void Subscribe()
    {
        base.Subscribe();
        _settingButton.onClick.AddListener(ViewModel.OpenSetting);
    }

    public override void Unsubscribe()
    {
        base.Unsubscribe();
        _settingButton.onClick.RemoveListener(ViewModel.OpenSetting);
    }

}
