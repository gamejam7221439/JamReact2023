﻿using WKosArch.UIService.Views.Windows;

public class SettingButtonViewModel : WindowViewModel
{
    protected override void AwakeInternal()
    {
        base.AwakeInternal();
    }

    internal void OpenSetting()
    {
        UI.ShowWindow<MainMenuWindowModel>();

        Window.Hide();
    }
}
