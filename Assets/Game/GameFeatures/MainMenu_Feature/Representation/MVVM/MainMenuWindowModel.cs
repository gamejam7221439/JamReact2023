﻿using WKosArch.Services.SoundService;
using WKosArch.UIService.Views.Windows;

public partial class MainMenuWindowModel : WindowViewModel
{
    internal void OpenCredditWindow() => 
        UI.ShowWindow<CreditWindowModel>();

    internal void OpenQuitGameWindow() => 
        UI.ShowWindow<ExitGameWindowModel>();

    internal void OpenRestartWindow() => 
        UI.ShowWindow<RestartWindowModel>();

    internal void OpenSoundWindow() => 
        UI.ShowWindow<SoundSettingViewModel>();
}
