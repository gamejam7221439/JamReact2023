﻿using UnityEngine;
using UnityEngine.UI;
using WKosArch.UIService.Views.Windows;

public class WindowMainMenu : Window<MainMenuWindowModel>
{
    [Space]
    [SerializeField] protected Button CredditButton;
    [SerializeField] protected Button SoundButton;
    [SerializeField] protected Button QuitButton;
    [SerializeField] protected Button RestartButton;

    public override void Subscribe()
    {
        base.Subscribe();
        CredditButton.onClick.AddListener(ViewModel.OpenCredditWindow);
        SoundButton.onClick.AddListener(ViewModel.OpenSoundWindow);
        QuitButton.onClick.AddListener(ViewModel.OpenQuitGameWindow);
        RestartButton.onClick.AddListener(ViewModel.OpenRestartWindow);
    }

    public override void Unsubscribe()
    {
        base.Unsubscribe();
        CredditButton.onClick.RemoveListener(ViewModel.OpenCredditWindow);
        SoundButton.onClick.RemoveListener(ViewModel.OpenSoundWindow);
        QuitButton.onClick.RemoveListener(ViewModel.OpenQuitGameWindow);
        RestartButton.onClick.RemoveListener(ViewModel.OpenRestartWindow);
    }
}
